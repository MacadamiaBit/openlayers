import tornado.ioloop
import tornado.web
import os

class MainHandler(tornado.web.RequestHandler):
    def get(self):
      	 self.render("kml-earthquakes.html")

class KMLHandler(tornado.web.RequestHandler):
    def get(self):
      	 self.render("data/eq.KML")

class KMLPAGEHandler(tornado.web.RequestHandler):
    def get(self):
      	 self.render("kml.html")

samplekmlpath = 'data/eq.KML'

settings = {
	"static_path": os.path.join(os.path.dirname(__file__), "data"),}

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/KML", KMLHandler),
    (r"/KMLPAGE", KMLPAGEHandler),
	], **settings)

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
