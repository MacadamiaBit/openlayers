// goog.require('ol.Map');
// goog.require('ol.RendererHint');
// goog.require('ol.View2D');
// goog.require('ol.expr');
// goog.require('ol.layer.Tile');
// goog.require('ol.layer.Vector');
// goog.require('ol.parser.KML');
// goog.require('ol.source.Stamen');
// goog.require('ol.source.Vector');
// goog.require('ol.style.Fill');
// goog.require('ol.style.Shape');
// goog.require('ol.style.Stroke');
// goog.require('ol.style.Style');



var style = new ol.style.Style({
  symbolizers: [
    new ol.style.Shape({
      //size: ol.expr.parse('5 + 20 * (magnitude - 5)'),
      fill: new ol.style.Fill({
        color: '#ff9900',
        opacity: 0.4
      }),
      stroke: new ol.style.Stroke({
        color: '#ffcc00',
        opacity: 0.2
      })
    })
  ]
});

var style2 =  new ol.style.Style({rules: [
            new ol.style.Rule({
              symbolizers: [
                new ol.style.Fill({
                  color: 'white',
                  opacity: 0.6
                }),
                new ol.style.Stroke({
                  color: '#319FD3',
                  opacity: 1
                })
              ]
            }),
            new ol.style.Rule({
              maxResolution: 5000,
              symbolizers: [
                new ol.style.Text({
                  color: 'black',
                  text: ol.expr.parse('description'),
                  fontFamily: 'Calibri,sans-serif',
                  fontSize: 12,
                  stroke: new ol.style.Stroke({
                    color: 'white',
                    width: 3
                  })
                })
              ]
            })
          ]})

var vector = new ol.layer.Vector({
  source: new ol.source.Vector({
    parser: new ol.parser.KML({
      maxDepth: 2, 
      extractStyles: true, 
      extractAttributes: true
    }),
    url: 'KML',

  }),
  style: style
});

var raster = new ol.layer.Tile({
  source: new ol.source.Stamen({
    layer: 'toner'
  })
});

var map = new ol.Map({
  layers: [raster, vector],
  renderer: ol.RendererHint.CANVAS,
  target: 'map',
  view: new ol.View2D({
    center: [0, 0],
    zoom: 2
  })
});

var info = $('#info');
info.tooltip({
  animation: false,
  trigger: 'manual'
});

map.on(['click'], function(evt) {
  var pixel = evt.getPixel();
  info.css({
    left: pixel[0] + 'px',
    top: (pixel[1] - 15) + 'px'
  });
  map.getFeatures({
    pixel: pixel,
    layers: [vector],
    success: function(layerFeatures) {
      var feature = layerFeatures[0][0];
      if (feature) {
        info.tooltip('hide')
            .attr('data-original-title', "Data: " + 
              //feature.attributes 
              feature.get('name')
              )
            .tooltip('fixTitle')
            .tooltip('show');
      } else {
        info.tooltip('hide');
      }
    }
  });
});
